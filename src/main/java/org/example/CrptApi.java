package org.example;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class CrptApi {

    private static final String API_URL = "https://ismp.crpt.ru/api/v3/lk/documents/create";
    private final HttpClient httpClient;
    private final ObjectMapper objectMapper;
    private final Semaphore semaphore;
    private final ScheduledExecutorService scheduler;


    public CrptApi(int requestLimit, TimeUnit timeUnit) {
        this.httpClient = HttpClient.newHttpClient();
        this.objectMapper = new ObjectMapper();
        this.semaphore = new Semaphore(requestLimit);
        this.scheduler = Executors.newScheduledThreadPool(1);

        scheduler.scheduleAtFixedRate(() -> semaphore.release(requestLimit - semaphore.availablePermits()),
                0, 1, timeUnit);

    }

    public void createDocument(String url, Document document, String signature) throws InterruptedException {
        semaphore.acquire();
        try {
            ObjectNode documentNode = objectMapper.valueToTree(document);// преобразует document в JSON
            documentNode.put("signature", signature); // добавляет подпись в JSON

            HttpRequest request = HttpRequest.newBuilder()  // создаем post запрос
                    .uri(URI.create(API_URL))
                    .header("Content-Type", "application/json")
                    .POST(HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(documentNode)))
                    .build();

            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() == 200) {
                System.out.println("Документ создан");
            } else {
                System.err.println("Ошибка при создании документа. HTTP-статус: " + response.statusCode());
            }
        } catch (InterruptedException | IOException e) {

            System.err.println("Ошибка при отправке запроса: " + e.getMessage());
        } finally {
            semaphore.release();
        }
    }

    public static void main(String[] args) {


        CrptApi crptApi = new CrptApi(10, TimeUnit.SECONDS);
        Description description = new Description("string");
        Product product = new Product("string", "2020-01-23",
                "string", "string", "string",
                "2020-01-23", "string", "string", "string");
        List<Product> products = new ArrayList<>();
        products.add(product);
        Document document = new Document(
                description,
                "string",
                "string",
                "LP_INTRODUCE_GOODS",
                true,
                "string",
                "string",
                "string",
                "2020-01-23",
                "string",
                (ArrayList<Product>) products,
                "2020-01-23",
                "string"
        );

        try {
            String signature = "example_signature";
            crptApi.createDocument(API_URL, document, signature);
            System.out.println("Документ создан");
            System.out.println(document);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Description {
        public String participantInn;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Product {
        public String certificate_document;
        public String certificate_document_date;
        public String certificate_document_number;
        public String owner_inn;
        public String producer_inn;
        public String production_date;
        public String tnved_code;
        public String uit_code;
        public String uitu_code;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Document {
        public Description description;
        public String doc_id;
        public String doc_status;
        public String doc_type;
        public boolean importRequest;
        public String owner_inn;
        public String participant_inn;
        public String producer_inn;
        public String production_date;
        public String production_type;
        public ArrayList<Product> products;
        public String reg_date;
        public String reg_number;
    }
}

